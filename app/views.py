from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import Schedule

# Create your views here.


def index(request):
    return render(request, 'landing.html')


def education(request):
    return render(request, 'education.html')


def skill(request):
    return render(request, 'skill.html')


def experience(request):
    return render(request, 'experience.html')


def contact(request):
    return render(request, 'contact.html')


def register(request):
    return render(request, 'register.html')


def add(request):
    if(request.method == 'POST'):
        data = dict(request.POST.items())
        del data['csrfmiddlewaretoken']
        try:
            Schedule(**data).save()
            return HttpResponseRedirect('/schedule')
        except:
            pass

    return render(request, 'schedule-add.html')


def schedule(request):
    data = {} if not Schedule.objects.all() else {'list': Schedule.objects.all()}
    return render(request, 'schedule.html', data)


def delete(request):
    Schedule.objects.all().delete()
    return HttpResponseRedirect('/schedule')
