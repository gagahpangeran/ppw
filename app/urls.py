from django.urls import path, re_path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('education', views.education, name='education'),
    path('skill', views.skill, name='skill'),
    path('experience', views.experience, name='experience'),
    path('contact', views.contact, name='contact'),
    path('register', views.register, name='register'),
    path('schedule', views.schedule, name='schedule'),
    path('schedule/add', views.add, name='add'),
    path('schedule/delete', views.delete, name='add')
]
