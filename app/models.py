from django.db import models

# Create your models here.


class Schedule(models.Model):
    activity = models.CharField(max_length=30)
    date = models.DateField()
    time = models.TimeField()
    place = models.CharField(max_length=30)
    category = models.CharField(max_length=30)
